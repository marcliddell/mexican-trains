package mexicantrains.snowytracks.com.mexicantrains.DominoUtils;

import android.os.AsyncTask;
import android.support.annotation.WorkerThread;
import android.util.Log;

import java.util.ArrayList;

import mexicantrains.snowytracks.com.mexicantrains.Model.Domino;

/**
 * This class can perform calcualtions on a set of dominos to find longest and highest trains of a set
 * Author marcliddell
 */
public class TrainCalculator {

    public interface ICalculatorListener {
        void onCalculationComplete();
    }

    private static final String TAG = TrainCalculator.class.getSimpleName();
    private ArrayList<Domino> longestTrain;
    private ArrayList<Domino> highPointsTrain;
    private ArrayList<Domino> mDominos;
    private int highestPoints = 0;
    private int i = 0;
    private static final int iMAX = 1000 * 1000 * 1000; //1 billion maximum iterations
    private int startingDouble = 0;
    private ArrayList<ICalculatorListener> mCalculatorListeners;

    public TrainCalculator() {
        longestTrain = new ArrayList<>();
        highPointsTrain = new ArrayList<>();
        mCalculatorListeners = new ArrayList<>();
    }

    public void addCalculateCompleteListener(ICalculatorListener listener) {
        mCalculatorListeners.add(listener);
    }

    public void removeCalculateCompleteListener(ICalculatorListener listener) {
        mCalculatorListeners.remove(listener);
    }


    public void setDominos(ArrayList<Domino> newDominos) {
        mDominos = newDominos;
        longestTrain.clear();
        highPointsTrain.clear();
        highestPoints = 0;
        i = 0;
    }

    public ArrayList<Domino> getLongestTrain() {
        return longestTrain;
    }

    public ArrayList<Domino> getHighestTrain() {
        return highPointsTrain;
    }

    /**
     * Call this method to calculate the train, then call the various getter methods to get the different trains
     */
    public void calculate() {
        //Create the starting double as the first domino for the train
        final ArrayList<Domino> startingDominos = new ArrayList<>();
        startingDominos.add(new Domino(startingDouble, startingDouble));

        Log.d(TAG, "Train calculator starting double " + startingDouble);


        new CalculateTrainTask(startingDominos).execute();

    }

    private class CalculateTrainTask extends AsyncTask<Void, Void, Void> {

        ArrayList<Domino> mStartingDominos;

        public CalculateTrainTask(ArrayList<Domino> startingDominos) {
            mStartingDominos = startingDominos;
        }


        @Override protected Void doInBackground(Void... params) {
            calculateLongestTrain(mDominos, mStartingDominos);
            if (i > 0) {
                Log.d(TAG, "Train calculator went to " + i + "recursions");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            onCalculationComplete();
        }


    }

    private void onCalculationComplete() {

        //Remove starting double domino
        longestTrain.remove(new Domino(startingDouble, startingDouble));
        highPointsTrain.remove(new Domino(startingDouble, startingDouble));

        for (ICalculatorListener listener : mCalculatorListeners) {
            listener.onCalculationComplete();
        }
    }

    public void setTrainStartingDouble(int number) {
        startingDouble = number;
    }

    /**
     * This is a recursive method to calculate a train of dominos from a given set of dominos
     *
     * @param remainingDominos The remaining dominos that can be added to the current train
     * @param currentTrain     The current train of dominos laid out
     */
    @WorkerThread private void calculateLongestTrain(ArrayList<Domino> remainingDominos, ArrayList<Domino>
            currentTrain) {

        for (Domino domino : remainingDominos) {
            //protection code to stop recursion going infinite incase of debugging
//            i++;
//            if (i > iMAX) {
//                Log.w(TAG, "Recursion got too deep, im getting out!!!");
//                break;
//            }

            //if the 2nd domino is a matching domino, rotate it, so the next if check is simpler
            if (domino.getDomino2() == currentTrain.get(currentTrain.size() - 1).getDomino2()) {
                domino.rotate();
            }

            if (domino.getDomino1() == currentTrain.get(currentTrain.size() - 1).getDomino2()) {

                //Create a new train domino list for the next level of recursion
                ArrayList<Domino> passOnTrain = new ArrayList<Domino>();
                for (Domino d3 : currentTrain) {
                    passOnTrain.add(new Domino(d3.getDomino1(), d3.getDomino2()));
                }
                //And add the domino that was matched
                passOnTrain.add(domino);

                //Create a new remaining domino List for the next level of recursion
                ArrayList<Domino> passOnRemaining = new ArrayList<Domino>();
                for (Domino d2 : remainingDominos) {
                    passOnRemaining.add(new Domino(d2.getDomino1(), d2.getDomino2()));
                }
                //And remove the domino that was matched
                passOnRemaining.remove(domino);


                //Save the train at this level, because we dont know if the next level will find another domino
                if (passOnTrain.size() > longestTrain.size()) {
                    longestTrain.clear();
                    for (Domino dominoCurrent : passOnTrain) {
                        longestTrain.add(new Domino(dominoCurrent.getDomino1(), dominoCurrent.getDomino2()));
                    }
                }
                if (DominoSetUtils.calculateTrainPoints(passOnTrain) > highestPoints) {
                    highPointsTrain.clear();
                    highestPoints = DominoSetUtils.calculateTrainPoints(passOnTrain);
                    for (Domino dominoCurrent : passOnTrain) {
                        highPointsTrain.add(new Domino(dominoCurrent.getDomino1(), dominoCurrent.getDomino2()));
                    }
                }

                //Recursively call this with the matched domino in the train and it removed from the remaining dominos
                calculateLongestTrain(passOnRemaining, passOnTrain);
            }
        }

    }
}
