package mexicantrains.snowytracks.com.mexicantrains.UI.Activities;

import mexicantrains.snowytracks.com.mexicantrains.UI.Views.BaseDominoView;

/**
 * Created by marcliddell on 11/01/16.
 */
public interface IDominoKeyboard {

    void showDominoKeyboardView(BaseDominoView dominoView);

    void hideDominoKeyboardView();
}
