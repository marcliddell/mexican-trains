package mexicantrains.snowytracks.com.mexicantrains.Config;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Class for storing app settings
 * <p/>
 * Created by Marc Liddell
 */
public class AppConfig extends StoredConfiguration {

    private static final String TAG = AppConfig.class.getSimpleName();

    // Preference keys
    private static final String DOMINO_KEYBOARD = "dominoKeyboard";

    private static volatile AppConfig sInstance;

    private boolean mUseDominoKeyboard;

    private AppConfig(Context context) {
        super(context, context.getSharedPreferences(AppConfig.class.getSimpleName(), Context.MODE_PRIVATE));
    }

    public static AppConfig get(Context context) {
        synchronized (AppConfig.class) {
            if (sInstance == null) {
                sInstance = new AppConfig(context.getApplicationContext());
            }
        }
        return sInstance;
    }

    @Override protected void onReadConfiguration(SharedPreferences prefs) {
        mUseDominoKeyboard = prefs.getBoolean(DOMINO_KEYBOARD, false);
    }

    @Override protected void onSaveConfiguration(SharedPreferences.Editor editor) {
        editor.putBoolean(DOMINO_KEYBOARD, mUseDominoKeyboard);
    }

    public boolean getUseDominoKeyboard() {
        return mUseDominoKeyboard;
    }

    public AppConfig setUseDominoKeyboard(boolean useDominoKeyboard) {
        mUseDominoKeyboard = useDominoKeyboard;
        return this;
    }

    @Override public void clearConfiguration() {
        super.clearConfiguration();
        mUseDominoKeyboard = false;
    }
}
