package mexicantrains.snowytracks.com.mexicantrains.UI.Activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import mexicantrains.snowytracks.com.mexicantrains.R;
import mexicantrains.snowytracks.com.mexicantrains.UI.Fragments.FragmentCalculateTrain;
import mexicantrains.snowytracks.com.mexicantrains.UI.Fragments.FragmentKeyboard;
import mexicantrains.snowytracks.com.mexicantrains.UI.Fragments.SettingsFragment;
import mexicantrains.snowytracks.com.mexicantrains.UI.Views.BaseDominoView;

/**
 * This is the main activity for the app - it just holds important fragments
 * Author Marc Liddell
 */
public class MainActivity extends AppCompatActivity implements IDominoKeyboard, ILoading {
    private static final String TAG = MainActivity.class.getSimpleName();

    @Bind(R.id.ll_fragment) protected LinearLayout fragmentLayout;
    @Bind(R.id.keyboard_fragment) protected LinearLayout keyboardLayout;
    @Bind(R.id.rl_loading) protected RelativeLayout rlLoading;
    private FragmentKeyboard keyboard;


    @Override public void showLoadingSpinner() {
        rlLoading.setVisibility(View.VISIBLE);
    }

    @Override public void hideLoadingSpinner() {
        rlLoading.setVisibility(View.GONE);
    }

    private enum DISPLAY_STATE {HOME, SETTINGS}

    private DISPLAY_STATE displayState = DISPLAY_STATE.HOME;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        FragmentManager fragMan = getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragMan.beginTransaction();
        FragmentCalculateTrain dominosFrag = FragmentCalculateTrain.newInstance();
        fragTransaction.add(fragmentLayout.getId(), dominosFrag, "main");
        fragTransaction.commit();

        keyboard = FragmentKeyboard.newInstance();
        FragmentTransaction fragTransaction2 = fragMan.beginTransaction();
        fragTransaction2.add(keyboardLayout.getId(), keyboard, "keyboard");
        fragTransaction2.commit();

    }

    @Override protected void onResume() {
        super.onResume();
        rlLoading.setVisibility(View.GONE);
    }

    @Override public void showDominoKeyboardView(BaseDominoView dominoView) {
        keyboard.setDominoKeyboardView(dominoView);
        showKeyboard();
    }

    @Override public void hideDominoKeyboardView() {
        hideKeyboard();
    }

    //Leave menu option out for now, until we make more settings for colors
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (displayState != DISPLAY_STATE.SETTINGS && id == R.id.action_settings) {
            displayState = DISPLAY_STATE.SETTINGS;
            FragmentManager fragMan = getSupportFragmentManager();
            FragmentTransaction fragTransaction = fragMan.beginTransaction();
            SettingsFragment settingsFragment = SettingsFragment.newInstance();
            fragTransaction.replace(R.id.ll_fragment, settingsFragment);
            fragTransaction.addToBackStack(null);

            fragTransaction.commit();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override public void onBackPressed() {
        if (keyboardLayout.getVisibility() == View.VISIBLE) {
            hideDominoKeyboardView();
            return;
        }
        displayState = DISPLAY_STATE.HOME;
        super.onBackPressed();
    }

    public void showKeyboard() {

        keyboardLayout.setVisibility(View.VISIBLE);
    }

    public void hideKeyboard() {
        keyboardLayout.setVisibility(View.GONE);
    }
}
