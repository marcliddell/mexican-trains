package mexicantrains.snowytracks.com.mexicantrains.Config;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Base class for storing permenat app config / data / settings
 * <p/>
 * Created by Marc Liddell on 21/10/2014.
 */
public abstract class StoredConfiguration {

    static final String TAG = StoredConfiguration.class.getSimpleName();

    private Context mContext;
    private SharedPreferences mPrefs;

    protected StoredConfiguration(Context context) {
        this.mContext = context;
        readConfiguration();
    }

    protected StoredConfiguration(Context context, SharedPreferences prefs) {
        this.mContext = context;
        this.mPrefs = prefs;
        readConfiguration();
    }

    protected abstract void onReadConfiguration(SharedPreferences prefs);

    protected abstract void onSaveConfiguration(SharedPreferences.Editor editor);

    protected Context getContext() {
        return mContext;
    }

    public final void readConfiguration() {
        SharedPreferences prefs = getSharedPreferences();
        onReadConfiguration(prefs);

        // If this is the first time we load the preference save what we have as defaults
        if (prefs.getAll() == null || prefs.getAll().isEmpty()) {
            saveConfiguration();
        }
    }

    public final void saveConfiguration() {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        onSaveConfiguration(editor);
        editor.commit();
    }

    public void clearConfiguration() {
        getSharedPreferences()
                .edit()
                .clear()
                .commit();

        // Read the configuration again to reset variables
        readConfiguration();
    }

    protected SharedPreferences getSharedPreferences() {
        if (mPrefs == null) {
            return PreferenceManager.getDefaultSharedPreferences(mContext);
        }
        return mPrefs;
    }

}
