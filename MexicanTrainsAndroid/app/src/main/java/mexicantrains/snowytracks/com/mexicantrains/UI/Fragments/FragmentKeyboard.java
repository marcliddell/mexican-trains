package mexicantrains.snowytracks.com.mexicantrains.UI.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.Bind;
import butterknife.ButterKnife;
import mexicantrains.snowytracks.com.mexicantrains.R;
import mexicantrains.snowytracks.com.mexicantrains.UI.Views.BaseDominoView;
import mexicantrains.snowytracks.com.mexicantrains.UI.Views.DominoKeyboard;

public class FragmentKeyboard extends Fragment {

    private static final String TAG = FragmentKeyboard.class.getSimpleName();
    @Bind(R.id.dk_domino_keyboard) protected DominoKeyboard dominoKeyboard;
    private BaseDominoView mDominoView;

    public FragmentKeyboard() {
        // Required empty public constructor
    }


    public static FragmentKeyboard newInstance() {
        return new FragmentKeyboard();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fragmentkeyboard, container, false);
        ButterKnife.bind(this, v);
        if (mDominoView != null) {
            dominoKeyboard.setDominoKeyboardView(mDominoView);
        }

        return v;
    }

    public void setDominoKeyboardView(BaseDominoView dominoView) {
        mDominoView = dominoView;
        if (dominoKeyboard != null) {
            dominoKeyboard.setDominoKeyboardView(dominoView);
        }
    }
}
