package mexicantrains.snowytracks.com.mexicantrains.DominoUtils;

import java.util.ArrayList;

import mexicantrains.snowytracks.com.mexicantrains.Model.Domino;
import mexicantrains.snowytracks.com.mexicantrains.Model.UIDomino;

/**
 * A class containing utility methods to be used on domino sets
 * Author marcliddell
 */
public class DominoSetUtils {

    /**
     * Given 2 sets, find which dominos are in the full set and not the subset
     *
     * @param fullSet of dominos (typically a users full domino hand)
     * @param subset  all of these dominos should exist in the full set
     * @return a subset of dominos which exist in the full set but do not exist in the subset
     */
    public static ArrayList<Domino> findRemainderDominos(ArrayList<Domino> fullSet, ArrayList<Domino> subset) {
        ArrayList<Domino> remainderDominos = new ArrayList<>();
        //Add all full set dominos into the remainder set
        for (Domino d : fullSet) {
            remainderDominos.add(new Domino(d.getDomino1(), d.getDomino2()));
        }

        //Remove all subset dominos from remainder set
        for (Domino d2 : subset) {
            remainderDominos.remove(new Domino(d2.getDomino1(), d2.getDomino2()));
        }

        return remainderDominos;
    }

    public static int calculateTrainPoints(ArrayList<Domino> dominos) {
        int total = 0;
        for (Domino domino : dominos) {
            total = total + domino.getDomino1();
            total = total + domino.getDomino2();
        }
        return total;
    }

    public static boolean doTrainsContainSameDominos(ArrayList<Domino> train1, ArrayList<Domino> train2) {

        if (train1.size() == train2.size()) {
            for (Domino domino : train1) {
                if (!train2.contains(domino)) {
                    return false;
                }
            }
        } else {
            return false;
        }

        return true;
    }

    public static ArrayList<Domino> changeDominosDeletable(ArrayList<Domino> dominoSet, boolean deletable) {

        //if the domino set is not a UI domino set, first convert it
        ArrayList<Domino> uiDominoSet = new ArrayList<>();
        if (dominoSet != null && dominoSet.size() > 0) {
            for (Domino domino : dominoSet) {
                uiDominoSet.add(new UIDomino(domino.getDomino1(), domino.getDomino2(), deletable));
            }
        }

        return uiDominoSet;
    }
}
