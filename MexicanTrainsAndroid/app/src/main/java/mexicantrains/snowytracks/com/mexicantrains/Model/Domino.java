package mexicantrains.snowytracks.com.mexicantrains.Model;

/**
 * The domino model class
 * Author marcliddell
 */
public class Domino {

    public static final int MIN = 0;
    public static final int MAX = 12;

    public Domino(int domino1, int domino2) {
        this.mDomino1 = domino1;
        this.mDomino2 = domino2;
    }

    private int mDomino1, mDomino2;

    public int getDomino1() {
        return mDomino1;
    }

    public int getDomino2() {
        return mDomino2;
    }

    @Override public boolean equals(Object o) {
        if (mDomino1 == ((Domino) o).getDomino1() && mDomino2 == ((Domino) o).getDomino2()) {
            return true;
        } else if (mDomino1 == ((Domino) o).getDomino2() && mDomino2 == ((Domino) o).getDomino1()) {
            return true;
        }
        return false;
    }

    @Override public String toString() {
        return mDomino1 + "/" + mDomino2;
    }

    public void rotate() {
        int tempDomino = mDomino1;
        mDomino1 = mDomino2;
        mDomino2 = tempDomino;

    }
}
