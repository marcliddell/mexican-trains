package mexicantrains.snowytracks.com.mexicantrains.UI.Views;

import android.content.Context;
import android.util.AttributeSet;

import mexicantrains.snowytracks.com.mexicantrains.R;

/**
 * Created by marcliddell on 11/01/16.
 */
public class HorizontalDominoView extends BaseDominoView {


    public HorizontalDominoView(Context context) {
        super(context);
    }

    public HorizontalDominoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HorizontalDominoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public HorizontalDominoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    protected void init() {
        inflate(getContext(), R.layout.view_domino_horizontal, this);
        super.init();
    }
}
