package mexicantrains.snowytracks.com.mexicantrains.UI.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import mexicantrains.snowytracks.com.mexicantrains.R;

/**
 * Created by marcliddell on 11/01/16.
 */
public class DominoKeyboard extends LinearLayout implements View.OnClickListener {

    @Bind(R.id.iv_domino0) protected ImageView mDomino0;
    @Bind(R.id.iv_domino1) protected ImageView mDomino1;
    @Bind(R.id.iv_domino2) protected ImageView mDomino2;
    @Bind(R.id.iv_domino3) protected ImageView mDomino3;
    @Bind(R.id.iv_domino4) protected ImageView mDomino4;
    @Bind(R.id.iv_domino5) protected ImageView mDomino5;
    @Bind(R.id.iv_domino6) protected ImageView mDomino6;
    @Bind(R.id.iv_domino7) protected ImageView mDomino7;
    @Bind(R.id.iv_domino8) protected ImageView mDomino8;
    @Bind(R.id.iv_domino9) protected ImageView mDomino9;
    @Bind(R.id.iv_domino10) protected ImageView mDomino10;
    @Bind(R.id.iv_domino11) protected ImageView mDomino11;
    @Bind(R.id.iv_domino12) protected ImageView mDomino12;
    private BaseDominoView mDominoView;

    public DominoKeyboard(Context context) {
        super(context);
        init();
    }

    public DominoKeyboard(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DominoKeyboard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public DominoKeyboard(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        View v = inflate(getContext(), R.layout.view_domino_keyboard, this);
        ButterKnife.bind(this, v);
        mDomino0.setOnClickListener(this);
        mDomino1.setOnClickListener(this);
        mDomino2.setOnClickListener(this);
        mDomino3.setOnClickListener(this);
        mDomino4.setOnClickListener(this);
        mDomino5.setOnClickListener(this);
        mDomino6.setOnClickListener(this);
        mDomino7.setOnClickListener(this);
        mDomino8.setOnClickListener(this);
        mDomino9.setOnClickListener(this);
        mDomino10.setOnClickListener(this);
        mDomino11.setOnClickListener(this);
        mDomino12.setOnClickListener(this);
    }

    public void setDominoKeyboardView(BaseDominoView dominoView) {

        //Clear the previous domino
        if (mDominoView != null && mDominoView != dominoView) {
            mDominoView.clearSelected();
        }
        mDominoView = dominoView;
    }

    @Override protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        //When the keyboard is dismissed, remove selected domino
        if (visibility == GONE && mDominoView != null) {
            mDominoView.clearSelected();
        }
    }

    @Override public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_domino0:
                setSelectedNumber(0);
                break;
            case R.id.iv_domino1:
                setSelectedNumber(1);
                break;
            case R.id.iv_domino2:
                setSelectedNumber(2);
                break;
            case R.id.iv_domino3:
                setSelectedNumber(3);
                break;
            case R.id.iv_domino4:
                setSelectedNumber(4);
                break;
            case R.id.iv_domino5:
                setSelectedNumber(5);
                break;
            case R.id.iv_domino6:
                setSelectedNumber(6);
                break;
            case R.id.iv_domino7:
                setSelectedNumber(7);
                break;
            case R.id.iv_domino8:
                setSelectedNumber(8);
                break;
            case R.id.iv_domino9:
                setSelectedNumber(9);
                break;
            case R.id.iv_domino10:
                setSelectedNumber(10);
                break;
            case R.id.iv_domino11:
                setSelectedNumber(11);
                break;
            case R.id.iv_domino12:
                setSelectedNumber(12);
                break;

        }
    }

    private void setSelectedNumber(int i) {
        if (mDominoView.isLeftSideSelected()) {
            mDominoView.setLeftSideNumber(i);
            mDominoView.setRightSideSelected();
        } else {
            mDominoView.setRightSideNumber(i);
        }
    }
}
