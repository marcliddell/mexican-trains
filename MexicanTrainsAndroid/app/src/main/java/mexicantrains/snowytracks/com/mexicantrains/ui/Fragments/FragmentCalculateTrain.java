package mexicantrains.snowytracks.com.mexicantrains.UI.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mexicantrains.snowytracks.com.mexicantrains.DominoUtils.DominoSetUtils;
import mexicantrains.snowytracks.com.mexicantrains.DominoUtils.TrainCalculator;
import mexicantrains.snowytracks.com.mexicantrains.Model.Domino;
import mexicantrains.snowytracks.com.mexicantrains.Model.UIDomino;
import mexicantrains.snowytracks.com.mexicantrains.R;
import mexicantrains.snowytracks.com.mexicantrains.UI.Activities.IDominoKeyboard;
import mexicantrains.snowytracks.com.mexicantrains.UI.Activities.ILoading;
import mexicantrains.snowytracks.com.mexicantrains.UI.Views.BaseDominoView;
import mexicantrains.snowytracks.com.mexicantrains.UI.Views.SectionGridRecycler;
import mexicantrains.snowytracks.com.mexicantrains.UI.Views.VerticalDominoView;

/**
 * This Fragment contains a UI to calculate a Domino Train
 * Author Marc Liddell
 */
public class FragmentCalculateTrain extends Fragment implements TrainCalculator.ICalculatorListener, BaseDominoView.IDominoViewListener {

    private static final String CLEAR_DIALOG_NAME = "clear_dialog";
    private static final String TAG = FragmentCalculateTrain.class.getSimpleName();
    @Bind(R.id.btn_calculate_train) protected TextView btnCalculateTrain;
    @Bind(R.id.btn_edit_dominos) protected TextView btnEditDominos;
    @Bind(R.id.btn_add_domino) protected ImageButton ivAddDomino;
    @Bind(R.id.iv_clear_dominos) protected Button ivClearDominos;
    @Bind(R.id.recycler_view) protected RecyclerView mRecyclerView;
    @Bind(R.id.tv_no_dominos) protected TextView tvNoDominos;
    @Bind(R.id.domino_add) protected BaseDominoView mDominoInputBox;
    @Bind(R.id.dv_starting_domino) protected BaseDominoView mStartingNoDomino;
    @Bind(R.id.l_bottom_entry) protected LinearLayout mLayoutAddDomino;
    private TrainCalculator trainCalc;
    private ArrayList<Domino> mDominos;
    DominoAdapter dominoAdapter;
    SectionGridRecycler sectionsAdapter;

    public FragmentCalculateTrain() {
        // Required empty public constructor
    }

    public static FragmentCalculateTrain newInstance() {
        FragmentCalculateTrain fragment = new FragmentCalculateTrain();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fragment_calculate_train, container, false);
        ButterKnife.bind(this, v);
        trainCalc = new TrainCalculator();
        mDominos = new ArrayList<>();
        mStartingNoDomino.setIsDouble(true);

        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 4));
        dominoAdapter = new DominoAdapter(mDominos);
        sectionsAdapter = new SectionGridRecycler(getContext(), R.layout.section,
                R.id.section_text, mRecyclerView, dominoAdapter);
        mRecyclerView.setAdapter(sectionsAdapter);

        updateDominosDrawing(null, false);
        return v;
    }

    public void updateDominosDrawing(ArrayList<DominoSection> dominoSections, boolean scrollToBottom) {

        if (tvNoDominos != null && dominoSections != null && dominoSections.size() > 0) {

            tvNoDominos.setVisibility(View.GONE);
            //Scroll to the bottom after 200ms, it causes problems if you do it whilst data adapter is updating
            if (scrollToBottom && mDominos.size() > 0) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mRecyclerView.smoothScrollToPosition(mRecyclerView.getAdapter().getItemCount() - 1);
                    }
                }, 200);
            }
        } else {
            mRecyclerView.setVisibility(View.GONE);
            tvNoDominos.setVisibility(View.VISIBLE);
            return;
        }

        mRecyclerView.setVisibility(View.VISIBLE);
        //To display dominos in sections, we combine all dominos into 1 arraylist and position section titles at top
        // of the sections
        ArrayList<Domino> dominosToDisplay = new ArrayList<>();
        List<SectionGridRecycler.Section> sections = new ArrayList<SectionGridRecycler.Section>();

        for (DominoSection section : dominoSections) {
            sections.add(new SectionGridRecycler.Section(dominosToDisplay.size(), section.title));
            for (Domino domino : section.dominos) {
                dominosToDisplay.add(domino);
            }
        }

        dominoAdapter.updateDominos(dominosToDisplay);
        SectionGridRecycler.Section[] dummy = new SectionGridRecycler.Section[sections.size()];
        sectionsAdapter.setSections(sections.toArray(dummy));
        sectionsAdapter.notifyDataSetChanged();

    }

    @Override public void onDeleteClicked(Domino domino) {
        if (domino != null) {
            removeDomino(domino);
        }
    }

    private class DominoSection {
        String title;
        ArrayList<Domino> dominos;

        public DominoSection(String aTitle, ArrayList<Domino> aDominos) {
            title = aTitle;
            dominos = aDominos;
        }
    }


    @OnClick(R.id.iv_clear_dominos)
    public void btnClearDominos(View view) {
        ClearDominosDialogFragment clearDominosFragment = new ClearDominosDialogFragment();
        clearDominosFragment.setTargetFragment(FragmentCalculateTrain.this, 0);
        clearDominosFragment.show(getActivity().getSupportFragmentManager(), CLEAR_DIALOG_NAME);
    }

    @OnClick(R.id.btn_edit_dominos)
    public void btnEditDominos(View view) {
        ArrayList<DominoSection> dominoSections = new ArrayList<>();
        dominoSections.add(new DominoSection(getString(R.string.dominos), mDominos));
        updateDominosDrawing(dominoSections, true);
        btnEditDominos.setVisibility(View.GONE);
        btnCalculateTrain.setVisibility(View.VISIBLE);
        mLayoutAddDomino.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btn_calculate_train)
    public void btnCalculateTrain(View view) {
        if (mDominos.size() == 0) {
            Toast.makeText(getActivity(), getString(R.string.add_dominos_first), Toast.LENGTH_SHORT).show();
            return;
        }


        int startingNo = -1;
        try {
            startingNo = mStartingNoDomino.getLeftSideValue();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), getString(R.string.enter_valid_starting_number), Toast.LENGTH_SHORT).show();
        }

        if (startingNo < Domino.MIN || startingNo > Domino.MAX) {
            Toast.makeText(getActivity(), getString(R.string.enter_valid_starting_number), Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        trainCalc.setDominos(mDominos);
        trainCalc.setTrainStartingDouble(startingNo);

        trainCalc.calculate();
        ((ILoading) getActivity()).showLoadingSpinner();

        btnEditDominos.setVisibility(View.VISIBLE);
        btnCalculateTrain.setVisibility(View.GONE);
        mLayoutAddDomino.setVisibility(View.GONE);
        ((IDominoKeyboard) getActivity()).hideDominoKeyboardView();
    }

    @Override public void onResume() {
        super.onResume();
        ((IDominoKeyboard) getActivity()).hideDominoKeyboardView();
        trainCalc.addCalculateCompleteListener(this);
    }

    @Override public void onPause() {
        super.onPause();
        ((IDominoKeyboard) getActivity()).hideDominoKeyboardView();
        trainCalc.removeCalculateCompleteListener(this);
    }

    @Override public void onCalculationComplete() {
        ArrayList<DominoSection> sections = new ArrayList<>();

        //if they are the same length, we only show the high point train
        if (trainCalc.getHighestTrain().size() == trainCalc.getLongestTrain().size()) {

            ArrayList<Domino> remaining = DominoSetUtils.findRemainderDominos(mDominos, trainCalc.getHighestTrain());
            ArrayList<Domino> train = trainCalc.getHighestTrain();
            train = DominoSetUtils.changeDominosDeletable(train, true);
            sections.add(new DominoSection(getString(R.string.best_train) + " (" + train.size() + ")", train));
            sections.add(new DominoSection(getString(R.string.remaining_dominos) + " (" + remaining.size() + ")", remaining));

        } else {
            //else definetly show the longest train

            ArrayList<Domino> longestRemaining = DominoSetUtils.findRemainderDominos(mDominos, trainCalc.getLongestTrain());
            ArrayList<Domino> longestTrain = trainCalc.getLongestTrain();
            longestTrain = DominoSetUtils.changeDominosDeletable(longestTrain, true);

            sections.add(new DominoSection(getString(R.string.longest_train) + " (" + longestTrain.size() + ")", longestTrain));
            sections.add(new DominoSection(getString(R.string.longest_remaining_dominos) + " (" + longestRemaining.size() + ")",
                    longestRemaining));

            //and show the highest train if it is a different train to the longest train
            if (!DominoSetUtils.doTrainsContainSameDominos(trainCalc.getLongestTrain(), trainCalc.getHighestTrain())) {

                ArrayList<Domino> highestRemaining = DominoSetUtils.findRemainderDominos(mDominos, trainCalc.getHighestTrain());
                ArrayList<Domino> highestTrain = trainCalc.getHighestTrain();
                highestTrain = DominoSetUtils.changeDominosDeletable(highestTrain, true);
                sections.add(new DominoSection(getString(R.string.highest_train) + " (" + highestTrain.size() + ")", highestTrain));
                sections.add(new DominoSection(getString(R.string.highest_train_remaining) + " (" + highestRemaining.size() + ")", highestRemaining));
            }
        }

        updateDominosDrawing(sections, false);

        ((ILoading) getActivity()).hideLoadingSpinner();

    }

    private class DominoHolder extends RecyclerView.ViewHolder {
        private final VerticalDominoView mDominoView;
        private Domino mDomino;

        public DominoHolder(View itemView) {
            super(itemView);

            mDominoView = (VerticalDominoView) itemView.findViewById(R.id.dv_domino);
            mDominoView.addViewListener(FragmentCalculateTrain.this);
        }

        public void bindDomino(Domino domino) {
            mDomino = domino;
            mDominoView.setDomino(domino);
            //all list dominos are not editable, only deletable
            mDominoView.setEditable(false);
            if (domino instanceof UIDomino) {
                mDominoView.setDeleteable(((UIDomino) domino).isDeletable());
            }
        }

    }

    private class DominoAdapter extends RecyclerView.Adapter<DominoHolder> {
        private ArrayList<Domino> mDominos;

        public DominoAdapter(ArrayList<Domino> dominos) {
            mDominos = dominos;
        }

        public void updateDominos(ArrayList<Domino> dominos) {
            mDominos = dominos;
            notifyDataSetChanged();
        }

        @Override
        public DominoHolder onCreateViewHolder(ViewGroup parent, int pos) {
            return new DominoHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_domino, parent, false));
        }

        @Override
        public void onBindViewHolder(DominoHolder holder, int pos) {
            holder.bindDomino(this.mDominos.get(pos));
        }

        @Override
        public int getItemCount() {
            return mDominos.size();
        }
    }

    @OnClick(R.id.btn_add_domino)
    protected void addCurrentDomino() {
        try {
            int dominoNumber1, dominoNumber2;

            dominoNumber1 = mDominoInputBox.getLeftSideValue();
            dominoNumber2 = mDominoInputBox.getRightSideValue();

            Domino newDomino = new Domino(dominoNumber1, dominoNumber2);
            if (mDominos.contains(newDomino)) {
                Toast.makeText(getContext(), getString(R.string.you_have_already_added), Toast.LENGTH_SHORT).show();
                return;
            }
            if (newDomino.getDomino1() < Domino.MIN || newDomino.getDomino1() > Domino.MAX || newDomino.getDomino2()
                    < Domino.MIN || newDomino.getDomino2() > Domino.MAX) {
                Toast.makeText(getContext(), getString(R.string.enter_a_valid_domino), Toast.LENGTH_SHORT).show();
                return;
            }
            mDominos.add(newDomino);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getContext(), getString(R.string.enter_a_valid_domino), Toast.LENGTH_SHORT).show();
        }

        mDominoInputBox.clearAndSelect();
        ArrayList<DominoSection> dominoSections = new ArrayList<>();
        dominoSections.add(new DominoSection(getString(R.string.dominos) + " (" + mDominos.size() + ")", mDominos));
        updateDominosDrawing(dominoSections, true);
    }


    public void removeDomino(Domino domino) {
        mDominos.remove(domino);
        if (mDominos.size() > 0) {
            ArrayList<DominoSection> dominoSections = new ArrayList<>();
            dominoSections.add(new DominoSection(getString(R.string.dominos) + " (" + mDominos.size() + ")", mDominos));
            updateDominosDrawing(dominoSections, false);
        } else {
            updateDominosDrawing(null, false);
        }
        btnEditDominos.setVisibility(View.GONE);
        btnCalculateTrain.setVisibility(View.VISIBLE);
    }


    public static class ClearDominosDialogFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(getString(R.string.clear_all_dominos))
                    .setPositiveButton(getString(R.string.clear), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            ((FragmentCalculateTrain) getTargetFragment()).clearDominos();
                            ((FragmentCalculateTrain) getTargetFragment()).updateDominosDrawing(null, false);
                        }
                    })
                    .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
            return builder.create();
        }
    }

    private void clearDominos() {
        if (mDominos != null) {
            mDominos.clear();
        }
    }

}
