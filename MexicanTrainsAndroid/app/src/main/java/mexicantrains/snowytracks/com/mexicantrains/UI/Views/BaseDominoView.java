package mexicantrains.snowytracks.com.mexicantrains.UI.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mexicantrains.snowytracks.com.mexicantrains.Model.Domino;
import mexicantrains.snowytracks.com.mexicantrains.R;
import mexicantrains.snowytracks.com.mexicantrains.UI.Activities.IDominoKeyboard;

/**
 * Created by marcliddell on 11/01/16.
 */
public abstract class BaseDominoView extends LinearLayout {

    private static final String TAG = BaseDominoView.class.getSimpleName();
    @Bind(R.id.iv_domino1) protected ImageView mDomino1;
    @Bind(R.id.iv_domino2) protected ImageView mDomino2;
    @Bind(R.id.l_domino) protected RelativeLayout llDomino;
    @Bind(R.id.iv_delete) protected ImageView ivDelete;

    private int leftNumber = -1, rightNumber = -1;
    private boolean mIsDouble = false;
    private Domino mDomino;
    private ArrayList<IDominoViewListener> mListeners;

    public BaseDominoView(Context context) {
        super(context);
        init();
    }

    public BaseDominoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BaseDominoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public BaseDominoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    protected void init() {
        ButterKnife.bind(this, getRootView());
        mListeners = new ArrayList<>();

        getLeftSideView().setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                setLeftSideSelected();
                ((IDominoKeyboard) getContext()).showDominoKeyboardView(BaseDominoView.this);
            }
        });

        getRightSideView().setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                setRightSideSelected();
                ((IDominoKeyboard) getContext()).showDominoKeyboardView(BaseDominoView.this);
            }
        });
    }

    public void setEditable(boolean isEditable) {
        if (!isEditable) {
            getLeftSideView().setClickable(false);
            getRightSideView().setClickable(false);
        } else {
            getLeftSideView().setClickable(true);
            getRightSideView().setClickable(true);
        }
    }

    public void setDeleteable(boolean isDeleteable) {
        if (isDeleteable) {
            ivDelete.setVisibility(VISIBLE);
        } else {
            ivDelete.setVisibility(GONE);
        }
    }

    public void setIsDouble(boolean isDouble) {
        mIsDouble = isDouble;
    }

    public void setDomino(Domino domino) {
        mDomino = domino;
        mDomino1.setImageResource(getDominoDrawable(domino.getDomino1()));
        mDomino2.setImageResource(getDominoDrawable(domino.getDomino2()));
    }

    private int getDominoDrawable(int dominoNumber) {
        switch (dominoNumber) {
            case 0:
                return 0;
            case 1:
                return R.drawable.dom1;
            case 2:
                return R.drawable.dom2;
            case 3:
                return R.drawable.dom3;
            case 4:
                return R.drawable.dom4;
            case 5:
                return R.drawable.dom5;
            case 6:
                return R.drawable.dom6;
            case 7:
                return R.drawable.dom7;
            case 8:
                return R.drawable.dom8;
            case 9:
                return R.drawable.dom9;
            case 10:
                return R.drawable.dom10;
            case 11:
                return R.drawable.dom11;
            case 12:
                return R.drawable.dom12;
        }
        return R.drawable.dom1;

    }


    public void setLeftSideSelected() {
        mDomino1.setSelected(true);
        mDomino2.setSelected(false);
    }

    public void setRightSideSelected() {
        mDomino1.setSelected(false);
        mDomino2.setSelected(true);
    }

    public boolean isLeftSideSelected() {
        return mDomino1.isSelected();
    }

    public boolean isRightSideSelected() {
        return mDomino2.isSelected();
    }

    public View getLeftSideView() {
        return mDomino1;
    }

    public View getRightSideView() {
        return mDomino2;
    }

    public int getLeftSideValue() {
        return leftNumber;
    }

    public int getRightSideValue() {
        return rightNumber;
    }

    public void setLeftSideNumber(int number) {
        mDomino1.setImageResource(getDominoDrawable(number));
        leftNumber = number;
        if (mIsDouble && rightNumber != leftNumber) {
            setRightSideNumber(number);
        }
    }

    public void setRightSideNumber(int number) {
        mDomino2.setImageResource(getDominoDrawable(number));
        rightNumber = number;
        if (mIsDouble && rightNumber != leftNumber) {
            setLeftSideNumber(number);
        }
    }

    public void clearSelected() {
        mDomino1.setSelected(false);
        mDomino2.setSelected(false);
    }

    public void clearAndSelect() {
        mDomino1.setImageResource(0);
        mDomino2.setImageResource(0);
        leftNumber = -1;
        rightNumber = -1;
        setLeftSideSelected();
    }

    @OnClick(R.id.iv_delete)
    protected void deleteClicked() {
        for (IDominoViewListener listener : mListeners) {
            if (listener != null) {
                listener.onDeleteClicked(mDomino);
            }
        }
    }


    public interface IDominoViewListener {
        public void onDeleteClicked(Domino domino);
    }

    public void addViewListener(IDominoViewListener listener) {
        mListeners.add(listener);
    }

    public void removeViewListener(IDominoViewListener listener) {
        mListeners.remove(listener);
    }
}
