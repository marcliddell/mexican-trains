package mexicantrains.snowytracks.com.mexicantrains.Model;

/**
 * Created by marcliddell on 21/01/16.
 */
public class UIDomino extends Domino {

    private boolean mDeletable = false;

    public UIDomino(int domino1, int domino2) {
        super(domino1, domino2);
    }

    public UIDomino(int domino1, int domino2, boolean deletable) {
        super(domino1, domino2);
        mDeletable = deletable;
    }

    public void setDeletable(boolean deletable) {
        mDeletable = deletable;
    }

    public boolean isDeletable() {
        return mDeletable;
    }
}
