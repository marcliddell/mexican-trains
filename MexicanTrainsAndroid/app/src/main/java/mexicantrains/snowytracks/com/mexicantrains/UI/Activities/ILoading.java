package mexicantrains.snowytracks.com.mexicantrains.UI.Activities;

/**
 * Created by marcliddell on 11/01/16.
 */
public interface ILoading {

    void showLoadingSpinner();

    void hideLoadingSpinner();
}
