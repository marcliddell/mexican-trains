package mexicantrains.snowytracks.com.mexicantrains.UI.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import mexicantrains.snowytracks.com.mexicantrains.Config.AppConfig;
import mexicantrains.snowytracks.com.mexicantrains.R;

/**
 * Fragment for displaying settings
 * <p/>
 * Author: Marc Liddell
 */
public class SettingsFragment extends Fragment {

    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_settings, container, false);

        CheckBox cbDominoKeyboard = (CheckBox) v.findViewById(R.id.cb_use_domino_keyboard);
        cbDominoKeyboard.setChecked(AppConfig.get(getContext()).getUseDominoKeyboard());
        cbDominoKeyboard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                AppConfig.get(getContext()).setUseDominoKeyboard(isChecked);
            }
        });

        return v;
    }

    @Override public void onPause() {
        super.onPause();
        AppConfig.get(getContext()).saveConfiguration();
    }
}
