Mexican Train Calculator
============

![Logo](MexicanTrainsAndroid/app/src/main/res/drawable-xxhdpi/train_icon.png)

This app is designed to showcase some Android development including:

 * Use of Material Design principals
 * Custom Views
 * Examples of good Android and OO programming.
 * Use of recursion to solve a domino problem
 * Use of popular libraries appcompat, recyclerview, butterknife and support
 * Threading



License
-------

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
